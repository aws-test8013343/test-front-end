import type materials from "@/types/material";
import http from "./axios";
function getMaterials() {
  return http.get("/materials");
}

function getMaterialByID(id: number) {
  return http.get(`/materials/${id}`);
}

function getUnit(unit: string) {
  return http.get(`/materials/type/${unit}`);
}

function saveMaterial(Material: materials) {
  return http.post("/materials", Material);
}

function updateMaterial(id: number, Material: materials) {
  return http.patch(`/materials/${id}`, Material);
}

function daleteMaterial(id: number) {
  return http.delete(`/materials/${id}`);
}
export default {
  getMaterials,
  getMaterialByID,
  saveMaterial,
  updateMaterial,
  daleteMaterial,
  getUnit,
};
