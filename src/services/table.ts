import type Table from "@/types/Table";
import http from "./axios";
function getTables() {
  return http.get("/tables");
}

function getTable(id: number) {
  return http.get(`/tables/${id}`);
}

function saveTable(Table: Table) {
  return http.post("/tables", Table);
}

function updateTable(id: number, Table: Table) {
  return http.patch(`/tables/${id}`, Table);
}

function deleteTable(id: number) {
  return http.delete(`/tables/${id}`);
}
export default {
  getTables,
  getTable,
  saveTable,
  updateTable,
  deleteTable,
};
