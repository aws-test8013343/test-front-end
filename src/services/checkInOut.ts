import type CheckInOut from "@/types/checkInOut";
import http from "./axios";
function getCheckInOut() {
  return http.get("/checkinscheckoits");
}

function getCheckInOutByID(id: number) {
  return http.get(`/checkinscheckoits/${id}`);
}

function saveCheckInOut(employee: CheckInOut) {
  return http.post("/checkinscheckoits", employee);
}

function updateCheckInOut(id: number, employee: CheckInOut) {
  console.log("updated");
  return http.patch(`/checkinscheckoits/${id}`, employee);
}

function daleteCheckInOut(id: number) {
  return http.delete(`/checkinscheckoits/${id}`);
}
export default {
  getCheckInOut,
  updateCheckInOut,
  daleteCheckInOut,
  saveCheckInOut,
  getCheckInOutByID,
};
