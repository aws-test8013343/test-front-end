import type FoodQueue from "@/types/foodQueue";
import http from "./axios";
function getFoodQueue() {
  return http.get("/food-queue");
}

//เอา food-queue ทั้งหมด สถานะ = รอทำ
function getFoodQueueChef() {
  return http.get("/food-queue/getStatusFoodQueue");
}
//เอา food-queue ทั้งหมด ไปจัดอยู่ใน order เพื่อทำหน้า serve
function getFoodQueueWithOrder() {
  return http.get("/food-queue/findFoodQueueWithOrder");
}
//เอา food-queue ทั้งหมด ไม่สนสถานะ FoodQueue (Get All Food Queue ของ Order ที่ยังไม่ชำระ )
function getFoodQueueNotUnPaid(idTable: number) {
  return http.get(`/food-queue/findFoodQueueNotUnPaid/${idTable}`);
}
//เอา food-queue ทั้งหมด โดยใช้ order id
function getFoodQueueByOrderId(idOrder: string) {
  return http.get(`/food-queue/findFoodQueueByOrderId/${idOrder}`);
}

function getFoodQueueByStatus(idOrder: string, status: string) {
  // 854f330d-f9b7-42bd-8815-5c80c81af262
  return http.get(`/food-queue/findFoodQueueByStatus/${status}/${idOrder}`);
}

function saveFoodQueue(queue: FoodQueue) {
  console.log("save");
  return http.post("/food-queue", queue);
}

function updateFoodQueue(id: number, queue: FoodQueue) {
  return http.patch(`/food-queue/${id}`, queue);
}

function deleteFoodQueue(id: number) {
  return http.delete(`/food-queue/${id}`);
}

function getFoodQueueByID(id: number) {
  return http.get(`/food-queue/${id}`);
}

function getMyFoodQueue(chefName: string) {
  return http.get(`/food-queue/myFoodQueue/${chefName}`);
}

export default {
  getFoodQueueByStatus,
  getFoodQueue,
  saveFoodQueue,
  updateFoodQueue,
  deleteFoodQueue,
  getFoodQueueByID,
  getFoodQueueChef,
  getMyFoodQueue,
  getFoodQueueWithOrder,
  getFoodQueueNotUnPaid,
  getFoodQueueByOrderId,
};
