import type Food from "@/types/Food";
import http from "./axios";
function getFoods() {
  return http.get("foods");
}

function saveFood(food: Food & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", food.name);
  formData.append("price", `${food.price}`);
  formData.append("type", food.type);
  formData.append("info", food.info);
  formData.append("file", food.files[0]);
  // console.log(food.files[0]);
  return http.post("/foods", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

async function updateFood(id: number, food: Food & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", food.name);
  formData.append("price", `${food.price}`);
  formData.append("type", food.type);
  formData.append("info", food.info);
  // console.log(food.files[0]);

  formData.append("file", food.files[0]);
  return http.patch(`/foods/${id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function updateFoodStatus(id: number, food: Food) {
  return http.patch(`/foods/${id}`, food);
}

function deleteFood(id: number) {
  return http.delete(`/foods/${id}`);
}

function getFoodImage(id: number) {
  return http.get(`/foods/${id}/image`);
}

function getBestFood() {
  return http.get(`/foods/findFoodBestSale`);
}

function getBadFood(type: string) {
  return http.get(`/foods/findFoodBadSale/${type}`);
}

function getFoodByID(id: number) {
  return http.get(`/foods/${id}`);
}

function getType(type: string) {
  return http.get(`/foods/type/${type}`);
}
export default {
  getBestFood,
  getBadFood,
  getFoods,
  getFoodImage,
  getType,
  saveFood,
  updateFood,
  deleteFood,
  getFoodByID,
  updateFoodStatus,
};
