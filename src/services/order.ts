import type Order from "@/types/order";
import http from "./axios";
function getOrders() {
  return http.get("/orders");
}

function saveOrder(order: Order) {
  console.log("save");
  return http.post("/orders", order);
}

function updateOrder(id: string, order: Order) {
  return http.patch(`/orders/${id}`, order);
}
function addFood(id: string, order: Order) {
  return http.patch(`/orders/addFood/${id}`, order);
}
function updateConfirmOrderItem(id: number) {
  return http.patch(`/orders/updateConfirmOrderItem/${id}`);
}
function deleteOrder(id: string) {
  return http.delete(`/orders/${id}`);
}
function deleteOrderItem(id: number) {
  return http.delete(`/orders/orderItem/${id}`);
}
function getOrderByID(id: string) {
  return http.get(`/orders/${id}`);
}
function findOneOrder(id: string) {
  return http.get(`/orders/findOneOrder/${id}`);
}

function getAllOrderItems() {
  return http.get(`/orders/getAllOrderItem`);
}

function getTableByQr(id: string) {
  return http.get(`/orders/findTableByQr/${id}`);
}
function getAllOrderNP() {
  return http.get(`/orders/getStatusOrderNotPaid`);
}

function getAllOrderHaveP() {
  return http.get(`/orders/getStatusOrderHavePaid`);
}

function payOrderByCash(id: string, amount: number, total: number) {
  return http.patch(`/orders/paidOrderByCash/${id}/${amount}/${total}`);
}

export default {
  getAllOrderHaveP,
  getAllOrderNP,
  getOrders,
  saveOrder,
  updateOrder,
  deleteOrder,
  getOrderByID,
  getAllOrderItems,
  payOrderByCash,
  addFood,
  deleteOrderItem,
  updateConfirmOrderItem,
  findOneOrder,
  getTableByQr,
};
