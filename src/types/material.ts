export default interface Table {
  id?: number;
  name?: string;
  amount?: number;
  price?: number;
  unit?: string;
}
