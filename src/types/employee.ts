import type User from "./User";

export default interface Employee {
  id?: number;
  firstname: string;
  lastname: string;
  age: number;
  identityNum: string;
  email: string;
  birthday: string;
  role: string;
  hourrate: number;
  userID?: User;
  // option: Option[];
}

// type Option = {
//   name: string;
//   price: number;
// };
