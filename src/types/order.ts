import type Employee from "./employee";
import type OrderItem from "./OrderItem";
import type Table from "./Table";

export default interface Order {
  id?: string;
  amount?: number;
  status?: string;
  table?: Table;
  employee?: Employee;
  orderItems?: OrderItem[];
  total?: number;
  timestampClose?: Date;
  timestampOpen?: Date;
}
