import type OrderItem from "./OrderItem";

export default interface FoodQueue {
  id?: number;
  name?: string;
  status: string;
  note?: string;
  chefName?: string;
  waiterName?: string;
  orderItem?: OrderItem;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
