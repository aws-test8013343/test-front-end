import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Table from "@/types/Table";
import tableService from "@/services/table";
import orderService from "@/services/order";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useTableStore = defineStore("table", () => {
  const indexSelected = ref(0);
  const getTableSelectedID = ref(3);
  const tablePaid = ref<Table>();
  const tables = ref<Table[]>([
    // {
    //   id: 0,
    //   name: "1",
    //   amount: 2,
    //   status: "พร้อมใช้งาน",
    // },
    // {
    //   id: 1,
    //   name: "2",
    //   amount: 2,
    //   status: "พร้อมใช้งาน",
    // },
    // {
    //   id: 2,
    //   name: "3",
    //   amount: 2,
    //   status: "พร้อมใช้งาน",
    // },
    // {
    //   id: 3,
    //   name: "4",
    //   amount: 2,
    //   status: "พร้อมใช้งาน",
    // },
    // {
    //   id: 4,
    //   name: "5",
    //   amount: 2,
    //   status: "พร้อมใช้งาน",
    // },
    // {
    //   id: 5,
    //   name: "6",
    //   amount: 2,
    //   status: "พร้อมใช้งาน",
    // },
    // {
    //   id: 6,
    //   name: "7",
    //   amount: 2,
    //   status: "พร้อมใช้งาน",
    // },
    // {
    //   id: 7,
    //   name: "8",
    //   amount: 2,
    //   status: "พร้อมใช้งาน",
    // },
    // {
    //   id: 8,
    //   name: "9",
    //   amount: 2,
    //   status: "พร้อมใช้งาน",
    // },
    {
      id: 0,
      name: "",
      amount: 0,
      status: "",
      order: [],
    },
  ]);

  const tableNP = ref<Table[]>([]);

  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);

  const editedTable = ref<Table & { files: File[] }>({
    name: "",
    amount: 0,
    status: "พร้อมใช้งาน",
    files: [],
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedTable.value = {
        name: "",
        amount: 0,
        status: "พร้อมใช้งาน",
        files: [],
      };
    }
  });

  async function getTables() {
    // loadingStore.isLoading = true;
    try {
      const res = await tableService.getTables();
      tables.value = res.data;
    } catch (e) {
      console.log(e);
    }
    // loadingStore.isLoading = false;
  }

  async function saveTable() {
    loadingStore.isLoading = true;
    try {
      if (editedTable.value.id) {
        const res = await tableService.updateTable(
          editedTable.value.id,
          editedTable.value
        );
      } else {
        const res = await tableService.saveTable(editedTable.value);
      }

      dialog.value = false;
      await getTables();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Table ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteTable(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await tableService.deleteTable(id);
      console.log(id);
      await getTables();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Table ได้");
    }
    loadingStore.isLoading = false;
  }

  function editTable(table: Table) {
    editedTable.value = JSON.parse(JSON.stringify(table));
    dialog.value = true;
  }

  function getTableSelected(): Table {
    const index = tables.value.findIndex(
      (item) => item.id === getTableSelectedID.value
    );
    console.log(tables.value[index]);
    return tables.value[index];
  }
  async function getTablesNotPaid() {
    // loadingStore.isLoading = true;
    try {
      const res = await tableService.getTables();
      tables.value = res.data;
    } catch (e) {
      console.log(e);
    }
    // loadingStore.isLoading = false;
  }

  return {
    tables,
    indexSelected,
    getTables,
    getTableSelected,
    saveTable,
    deleteTable,
    editTable,
    dialog,
    editedTable,
    getTablesNotPaid,
    getTableSelectedID,
  };
});
