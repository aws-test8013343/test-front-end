import { ref } from "vue";
import { defineStore } from "pinia";
import { useFoodStore } from "@/stores/foods";
import { useEmployeeStore } from "@/stores/employee";
import { useTableStore } from "@/stores/table";
import type OrderItem from "@/types/OrderItem";
import type Order from "@/types/order";
import type Employee from "@/types/employee";
// import type Table from "@/types/zzzz";
import tableService from "@/services/table";
import orderService from "@/services/order";
import employee from "@/services/employee";
import type FoodQueue from "@/types/foodQueue";
import foodQueueService from "@/services/foodQueue";
// import { usemanagePopupStore } from "@/stores/popupManagement";
// const popupStore = usemanagePopupStore();
// const foodstore = useFoodStore();
// const empStore = useEmployeeStore();
// const tableStore = useTableStore();

const editOrderItem = ref<OrderItem>({
  id: 1,
  name: "s",
  note: "ใส่น้ำแข็งเยอะๆ ใส่กล้วยด้วย",
  qty: 1,
  price: 20,
  // status: "รอทำ",
  // foodQueue: {
  //   name: "",
  //   status: "",
  // },
});
export const useOrderItemStore = defineStore("orderItem", () => {
  function addMyOrders(orderItem: OrderItem, id: number): void {
    const index = ordersItemCook.value.findIndex((item) => item.id === id);
    // orderItem.foodQueue!.status = "กำลังทำ";
    myOrders.value.push(orderItem);
    ordersItemCook.value.splice(index, 1);
  }

  function cancleOrder(id: number, orderItem: OrderItem): void {
    const index = ordersItem.value.findIndex((item) => item.id === id);
    // orderItem.foodQueue!.status = "ยกเลิก";
    ordersItemCook.value.splice(index, 1);
    //หักเงิกออกจากorder
  }

  function finish(myOrder: OrderItem, id: number): void {
    const index = myOrders.value.findIndex((item) => item.id === id);
    // myOrder.foodQueue!.status = "รอเสริฟ";
    //ต้องเพิ่มไปในรายการเสริฟ
    myOrders.value.splice(index, 1);
  }

  function cancleMyOrder(id: number, order: OrderItem): void {
    const index = myOrders.value.findIndex((item) => item.id === id);
    // order.foodQueue!.status = "ยกเลิก";
    myOrders.value.splice(index, 1);
    //หักเงิกออกจากorder
  }
  function deleteOrderItem(id: number): void {
    // const index = ordersItem.value.findIndex((item) => item.id === id);
    ordersItem.value.splice(id, 1);
  }
  let lastId = 1;
  function addOrderItem(orderItem: OrderItem) {
    orderItem.id = lastId++;
    ordersItem.value.push(orderItem);
    console.log(ordersItem.value.length);
  }

  function updateMyOrder(id: number, orderItem: OrderItem): void {
    // editOrderItem.value = { ...orderItem };
    ordersItem.value[id] = orderItem;
  }
  //เพิ่มออเดอร์ไอเทม
  const orderItemSelect = ref<OrderItem>({
    id: 1,
    name: "s",
    note: "ใส่น้ำแข็งเยอะๆ ใส่กล้วยด้วย",
    table: {
      status: "ว่าง",
    },
    qty: 1,
    price: 20,
    // status: "รอยืนยัน",
  });

  function addOrderItemSelect(item: OrderItem) {
    orderItemSelect.value = { ...item };
  }
  const orderItemSelectId = ref(0);
  const ordersItem = ref<OrderItem[]>([]);
  const listOrder = ref<OrderItem[]>([]);
  const ordersItemCook = ref<OrderItem[]>([]);

  // async function confirm(currentOrder: string, table: Table) {
  //   const sumPcs = 0;
  //   const sumPrice = 0;
  //   // const table = ordersRepository.
  //   // const table = orderItemSelect.value.table;
  //   const order: Order = {
  //     total: 10,
  //     amount: 10,
  //     id: currentOrder,
  //     table: table,
  //     // employee: empStore.employess[0],
  //     orderItems: [],
  //     status: "ยังไม่ชำระ",
  //   };

    // ordersItem.value.forEach(async (element, index) => {
    //   for (let index = 0; index < element.qty; index++) {
    //     // element.foodQueue!.status = "รอทำ";
    //     priceCurrentOrder.value += element.food?.price!;
    //     listOrder.value.push(element);
    //   }
  //   }, (order.orderItems = ordersItem.value)),
  //     orderService.updateOrder(currentOrder, order);
  //   console.log("currenTable: " + order.table?.name);
  //   ordersItem.value = [];
  // }
  // async function addFood(
  //   currentOrder: string,
  //   // table: Table,
  //   orderItem: OrderItem
  // ): Promise<boolean> {
  //   const sumPcs = 0;
  //   const sumPrice = 0;
  //   // const table = ordersRepository.
  //   // const table = orderItemSelect.value.table;
  //   const order: Order = {
  //     total: 10,
  //     amount: 10,
  //     id: currentOrder,
  //     // table: table,
  //     // employee: empStore.employess[0],
  //     orderItems: [orderItem],
  //     status: "ยังไม่ชำระ",
  //   };
    // console.log("add food");
    // const res = await tableService.getTable(table.id!);
    // console.log(res.data);
    // console.log("In function: " + res.data.name);
    // if (res.data.status === "กำลังใช้งาน") {
    //   await orderService.addFood(currentOrder, order);
    //   return true;
    // } else {
    //   console.log("โต๊ะไม่พร้อมใช้งานในขณะนี้");
    //   return false;
    // }

    //   ordersItem.value.forEach(async (element, index) => {
    //     for (let index = 0; index < element.qty; index++) {
    //       element.foodQueue!.status = "รอทำ";
    //       priceCurrentOrder.value += element.food?.price!;
    //       listOrder.value.push(element);
    //     }
    //   }, (order.orderItems = ordersItem.value)),
    //     orderService.updateOrder(currentOrder, order);
    //   console.log("currenTable: " + order.table?.name);
    //   ordersItem.value = [];
    // }
  // }
  const myOrders = ref<OrderItem[]>([]);
  const priceCurrentOrder = ref<number>(0);
  return {
    ordersItem,
    myOrders,
    addMyOrders,
    priceCurrentOrder,
    cancleOrder,
    cancleMyOrder,
    finish,
    addOrderItem,
    updateMyOrder,
    orderItemSelect,
    orderItemSelectId,
    addOrderItemSelect,
    deleteOrderItem,
    ordersItemCook,
    confirm,
    listOrder,
    // addFood,
  };
});
