import { ref } from "vue";
import { defineStore } from "pinia";
import type OrderItem from "@/types/OrderItem";
import orderService from "@/services/order";
import type Order from "@/types/order";
import type Table from "@/types/Table";
import foodQueueService from "@/services/foodQueue";
import type FoodQueue from "@/types/foodQueue";
import foodQueue from "@/services/foodQueue";

export const useOrderStore = defineStore("order", () => {
  const currentOrder = ref("");
  const order = ref<Order>();
  const myOrders = ref<OrderItem[]>([]);
  const ordersNP = ref<Order[]>([]);
  const ordersHasP = ref<Order[]>([]);
  const orderItems = ref<OrderItem[]>([]);
  const popupCashier = ref(false);
  const btnController = ref(true);
  const orderConfirmed = ref<Order>();
  const date = new Date();
  const tableByQr = ref<Order>();

  async function getTableByQr(id: string) {
    try {
      const res = await orderService.getTableByQr(id);
      tableByQr.value = res.data;
      return tableByQr.value;
    } catch (e) {
      console.log(e);
    }
  }
  const isPlus = ref(true);
  const orders = ref<Order[]>([
    {
      amount: 4,
      table: {
        name: "1",
        status: "ยังไม่ชำระ",
      },
      orderItems: [
        {
          name: "ข้าวผัด",
          price: 50,
          qty: 1,
          // status: "พร้อมเสริฟ",
          note: "ไม่ผัก ไม่พริก ไม่หวาน ไม่เส้น",
        },
        {
          name: "ข้าวผัด",
          price: 50,
          qty: 1,
          // status: "พร้อมเสริฟ",
          note: "",
        },
        {
          name: "ข้าวผัด",
          price: 50,
          qty: 1,
          // status: "พร้อมเสริฟ",
          note: "ไม่ผัก ไม่พริก ไม่หวาน ไม่เส้น",
        },
      ],
    },
  ]);

  function addMyOrders(order: OrderItem, id: string): void {
    const index = orders.value.findIndex((item) => item.id === id);
    myOrders.value.push(order);
    orders.value.splice(index, 1);
    //ต้องเพิ่มเปลี่ยนสถานะเป็นกำลังทำ
  }

  function cancleOrder(id: string): void {
    const index = orders.value.findIndex((item) => item.id === id);
    orders.value.splice(index, 1);
    //ต้องเพิ่มเปลี่ยนสถานะเป็นยกเลิก
  }

  // function finish(myOrder: OrderItem, id: string): void {
  //   const index = myOrders.value.findIndex((item) => item.id === id);
  //   //ต้องเพิ่มไปในรายการเสริฟ
  //   myOrders.value.splice(index, 1);
  //   //ต้องเพิ่มเปลี่ยนสถานะเป็นกำลังทำ
  // }

  function cancleMyOrder(id: number): void {
    const index = myOrders.value.findIndex((item) => item.id === id);
    myOrders.value.splice(index, 1);
    //ต้องเพิ่มเปลี่ยนสถานะเป็นยกเลิก
  }

  async function getOrders() {
    try {
      const res = await orderService.getOrders();
      orders.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }
  async function getOrderByID(id: string) {
    try {
      let count = 0;
      const res = await orderService.getOrderByID(id);
      order.value = res.data;
      order.value?.orderItems?.forEach((orderItem, index) => {
        if (orderItem.status === "ยืนยันแล้ว") {
          count++;
        }
      });
      if (count !== order.value?.orderItems?.length) {
        btnController.value = true;
      }
    } catch (e) {
      console.log(e);
    }
  }
  async function getOrderConfirmedByID(id: string) {
    try {
      let count = 0;
      const res = await orderService.findOneOrder(id);
      orderConfirmed.value = res.data;
      console.log(order.value);
      order.value?.orderItems?.forEach((orderItem, index) => {
        if (orderItem.status === "ยืนยันแล้ว") {
          count++;
        }
      });
      if (count !== order.value?.orderItems?.length) {
        btnController.value = true;
      }
    } catch (e) {
      console.log(e);
    }
  }
  function confirmOrder(orderConfirm: Order) {
    // orderItem.foodQueue?.forEach(async (foodQueue) => {
    //   foodQueue.status = "รอทำ";
    //   // foodQueueService.saveFoodQueue(foodQueue);
    //   console.log(foodQueue);
    // });
    // console.log(order);
    orderConfirm.orderItems?.forEach(async (orderItems) => {
      orderService.updateConfirmOrderItem(orderItems.id!);
      for (let index = 0; index < orderItems.qty; index++) {
        const foodQueue: FoodQueue = {
          status: "รอทำ",
          chefName: "",
          name: orderItems.name,
          note: orderItems.note,
          orderItem: orderItems,
          waiterName: "",
        };
        foodQueueService.saveFoodQueue(foodQueue);
      }
      // orderItems.foodQueue?.forEach((fq) => {
      //   fq.status = "รอทำ";
      //   console.log(fq);
      //   foodQueue.updateFoodQueue(fq.id!, fq);
      // });
      // const fq: FoodQueue = {
      //   status: "รอทำ",
      //   chefName: "",
      //   name: orderItems.name,
      //   note: orderItems.note,
      //   orderItem: orderItems,
      //   waiterName: "",
      // };
      // await foodQueueService.saveFoodQueue(fq);
    });
    order.value = {};
  }
  async function getOrdersNP() {
    try {
      const res = await orderService.getAllOrderNP();
      ordersNP.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }
  async function updateMyOrder(
    id: string,
    orderItem: OrderItem,
    oldOrder: Order
  ): Promise<void> {
    // editOrderItem.value = { ...orderItem };
    const order: Order = {
      total: 0,
      amount: 0,
      orderItems: [orderItem],
      status: "ยังไม่ชำระ",
      table: openedTable.value,
    };
    await orderService.updateOrder(id, order);
    let countTotal: number = 0;
    let countPiece: number = 0;
    oldOrder.orderItems?.forEach((element) => {
      countPiece += element.qty;
      countTotal += element.price;
    });
    console.log(countPiece);
    console.log(countTotal);
    console.log("update orderItem success");
    if (isPlus.value === true) {
      const order: Order = {
        total: 0,
        amount: 0,
        orderItems: [orderItem],
        status: "ยังไม่ชำระ",
        table: openedTable.value,
      };
      await orderService.updateOrder(id, order);
      console.log("update orderItem success");
    }
  }
  async function getOrdersHavePaid() {
    try {
      const res = await orderService.getAllOrderHaveP();
      ordersHasP.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }
  async function deleteOrderItem(id: number) {
    try {
      const res = await orderService.deleteOrderItem(id);
      console.log(res.data.price);
    } catch (e) {
      console.log(e);
    }
  }
  async function payOrderByCash(id: string, amount: number, total: number) {
    try {
      await orderService.payOrderByCash(id, amount, total);
    } catch (e) {
      console.log(e);
    }
  }

  const openedTable = ref<Table>();
  async function openOrder(tableSelected: Table) {
    openedTable.value = tableSelected;
    const order: Order = {
      total: 0,
      amount: 0,
      orderItems: [],
      status: "ยังไม่ชำระ",
      table: tableSelected,
    };
    const res = await orderService.saveOrder(order);
    console.log("opened table: " + tableSelected.id);
    currentOrder.value = res.data.id;
    await orderService.getOrders();
  }
  async function getAllOrderItemsChef() {
    try {
      const res = await orderService.getAllOrderItems();
      orderItems.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  // function checkSuccessServe(order: Order, allFoodQueue: number): number {
  //   let count: number = 0;

  //   const index = orders.value.findIndex((item) => item === order);
  //   order.orderItems!.forEach((orderItems) => {
  //     orderItems.foodQueue?.forEach((foodQueue) => {
  //       foodQueue.status === "เสิร์ฟสำเร็จ";
  //       count++;
  //     });
  //   });
  //   if (count === allFoodQueue) {
  //     orders.value.splice(index, 1);
  //   }
  //   count - 1;
  //   return count;
  // }

  function allFoodQueue(order: Order): number {
    let count = 0;
    order.orderItems?.forEach((element) => {
      count += element.qty;
    });
    return count;
  }

  // function calSale() {
  //   let sales = 0;
  //   const yesterday = new Date();

  //   yesterday.setDate(yesterday.getDate() - 1);
  //   // console.log(yesterday);
  //   // console.log(new Date());
  //   ordersHasP.value.forEach((order) => {
  //     // console.log(orderStore.ordersHasP[0].timestampClose);
  //     if (
  //       new Date(order.timestampClose!) <= date &&
  //       new Date(order.timestampClose!) >= yesterday
  //     ) {
  //       sales += order.total!;
  //     }
  //   });
  //   // console.log(sales);
  //   return sales;
  // }

  return {
    // calSale,
    payOrderByCash,
    getOrdersHavePaid,
    ordersHasP,
    popupCashier,
    ordersNP,
    getOrdersNP,
    orders,
    currentOrder,
    myOrders,
    allFoodQueue,
    addMyOrders,
    cancleOrder,
    cancleMyOrder,
    getOrderByID,
    orderConfirmed,
    // finish,
    getOrders,
    openOrder,
    // checkSuccessServe,
    openedTable,
    orderItems,
    getAllOrderItemsChef,
    order,
    btnController,
    updateMyOrder,
    deleteOrderItem,
    confirmOrder,
    getOrderConfirmedByID,
    getTableByQr,
    tableByQr,
    isPlus,
  };
});
