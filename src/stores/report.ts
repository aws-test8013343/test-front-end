import { ref } from "vue";
import { defineStore } from "pinia";
import type OrderItem from "@/types/OrderItem";
import orderService from "@/services/order";
import type Order from "@/types/order";
import type Table from "@/types/Table";
import foodQueueService from "@/services/foodQueue";
import type FoodQueue from "@/types/foodQueue";
import foodQueue from "@/services/foodQueue";
import { useOrderStore } from "./orders";
import { useFoodStore } from "./foods";

export const useReportStore = defineStore("report", () => {
  const date = new Date();
  const orderStore = useOrderStore();
  const foodStore = useFoodStore();
  const categories1 = ref<string[]>([]);
  const foodData = ref<number[]>([]);
  const categories2 = ref<string[]>([]);
  const foodBadData = ref<number[]>([]);

  function calBestFood() {
    // const dataTemp: number[] = [];
    // const foodDataTemp: string[] = [];
    let count: number = 0;
    if (foodData.value.length === 0) {
      foodStore.reportBestFood.forEach((food, index) => {
        if (foodStore.reportBestFood.length < 5) {
          categories1.value.push(food.name!);
          // console.log(food.orderItems);
          foodData.value.push(parseInt(food.totalQuantity));
        } else {
          if (count < 5) {
            categories1.value.push(food.name!);
            foodData.value.push(parseInt(food.totalQuantity));
          } else {
            return;
          }
          count++;
        }
      });
    }

    // foodData.value = dataTemp;
    // categories1.value = foodDataTemp;
  }

  function calBadFood() {
    // const dataTemp: number[] = [];
    // const foodDataTemp: string[] = [];
    let count: number = 0;
    // foodBadData.value = [];
    // console.log(foodBadData.v);
    if (foodBadData.value.length === 0) {
      foodStore.reportBadFood.forEach((food, index) => {
        console.log(food.name);
        if (foodStore.reportBestFood.length < 5) {
          categories2.value.push(food.name!);
          // console.log(food.orderItems);
          foodBadData.value.push(parseInt(food.totalQuantity));
        } else {
          if (count < 5) {
            categories2.value.push(food.name!);
            foodBadData.value.push(parseInt(food.totalQuantity));
          } else {
            return;
          }
          count++;
        }
      });
    }

    // categories2.value = foodDataTemp;
    // foodBadData.value = dataTemp;
    // console.log(categories2);
    // foodData.value = dataTemp;
    // categories1.value = ["sdsd", "sdds", "ssd", "sdds"];
  }

  const option2 = ref({
    chart: {
      id: "vuechart-example",
      toolbar: {
        show: false,
      },
    },
    xaxis: {
      categories: categories1,
    },
    series: [
      {
        name: "เมนูยอดฮิต",
        data: foodData,
      },
    ],
  });

  const option3 = ref({
    chart: {
      id: "vuechart-example",
      toolbar: {
        show: false,
      },
    },
    xaxis: {
      categories: categories2,
    },
    series: [
      {
        name: "เมนูยอดไม่ดี",
        data: foodBadData,
      },
    ],
  });

  function calSale() {
    // await orderStore.getOrdersHavePaid();
    // console.log(orderStore.ordersHasP);
    let sales = 0;
    const yesterday = new Date();

    yesterday.setDate(yesterday.getDate() - 1);
    // console.log(yesterday);
    // console.log(new Date());
    orderStore.ordersHasP.forEach((order) => {
      // console.log(orderStore.ordersHasP[0].timestampClose);

      sales += order.total!;
    });
    return sales;
  }

  return {
    categories2,
    foodBadData,
    calBadFood,
    calSale,
    option2,
    option3,
    calBestFood,
  };
});
