import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Food from "@/types/Food";
import foodService from "@/services/food";
import { useSelectStore } from "./selectMenu";
import { usemanagePopupStore } from "@/stores/popupManagement";

export const useFoodStore = defineStore("food", () => {
  const popupManagement = usemanagePopupStore();
  const dialog = ref(false);
  const deleteDialog = ref(false);
  const deleteItemId = ref();
  const deleteItemName = ref();
  const addFoodTypes = ref(["อาหาร", "ของหวาน", "เครื่องดื่ม"]);
  const foodTypes = ref(["ทั้งหมด", "อาหาร", "ของหวาน", "เครื่องดื่ม"]);
  const selectedFoodType = ref(foodTypes.value[0]);
  const foods = ref<Food[]>([]);
  const foodSelected = ref<Food>();
  const foodsByType = ref<Food[]>([]);
  const reportBestFood = ref<{ id: 0; name: ""; totalQuantity: "" }[]>([]);
  const reportBadFood = ref<{ id: 0; name: ""; totalQuantity: "" }[]>([]);
  const foodSelection = useSelectStore();
  const editedFood = ref<Food & { files: File[] }>({
    name: "",
    price: 0,
    type: "",
    status: true,
    files: [],
    info: "",
  });

  watch(selectedFoodType, () => {
    getBadFood(selectedFoodType.value);
    getFoodsByType();
  });
  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedFood.value = {
        name: "",
        price: 0,
        type: "",
        status: true,
        files: [],
        info: "",
      };
    }
  });

  function showDeleteDialog(id: number, name: string) {
    deleteItemId.value = id;
    deleteDialog.value = true;
    deleteItemName.value = name;
  }

  //For first time when logged id.
  async function getFoods() {
    try {
      const res = await foodService.getFoods();
      foods.value = res.data;
      if (selectedFoodType.value === "ทั้งหมด") {
        foodsByType.value = foods.value;
      }
    } catch (e) {
      console.log(e);
    }
  }
  //For sort type of food in manage menu screen.
  async function getFoodsByType() {
    if (selectedFoodType.value === "ทั้งหมด") {
      await getFoods();
    } else {
      try {
        const res = await foodService.getType(selectedFoodType.value);
        foodsByType.value = res.data;
      } catch (e) {
        console.log(e);
      }
    }
  }

  async function saveFood() {
    try {
      //use when edit food
      if (editedFood.value.id) {
        if (editedFood.value.files == undefined) {
          editedFood.value.files = [];
        }
        const res = await foodService.updateFood(
          editedFood.value.id,
          editedFood.value
        );
      }
      //use when add new food
      else {
        const res = await foodService.saveFood(editedFood.value);
      }
      dialog.value = false;
      getFoodsByType();
    } catch (e) {
      console.log(e);
    }
  }

  async function toggleFoodStatus(food: Food) {
    try {
      editedFood.value = JSON.parse(JSON.stringify(food));
      if (editedFood.value.id) {
        editedFood.value.status = !editedFood.value.status;
        const res = await foodService.updateFoodStatus(
          editedFood.value.id,
          editedFood.value
        );
      }
    } catch (e) {
      console.log(e);
    }
  }

  async function foodStatusFalse(food: Food) {
    try {
      //use when edit food
      editedFood.value = JSON.parse(JSON.stringify(food));
      if (editedFood.value.id) {
        editedFood.value.status = false;
        const res = await foodService.updateFoodStatus(
          editedFood.value.id,
          editedFood.value
        );
      }
    } catch (e) {
      console.log(e);
    }
  }

  function editFood(food: Food) {
    editedFood.value = JSON.parse(JSON.stringify(food));
    dialog.value = true;
  }

  async function deleteFood() {
    try {
      const res = await foodService.deleteFood(deleteItemId.value);
      deleteDialog.value = false;
      await getFoodsByType();
    } catch (e) {
      console.log(e);
    }
  }

  async function getBestFood() {
    try {
      const res = await foodService.getBestFood();
      reportBestFood.value = res.data;
      // reportBestFood.value[0].

      // await getFoodsByType();
    } catch (e) {
      console.log(e);
    }
  }

  async function getBadFood(type: string) {
    try {
      const res = await foodService.getBadFood(type);
      reportBadFood.value = res.data;
      // reportBestFood.value[0].
      console.log(reportBadFood.value);
      // await getFoodsByType();
    } catch (e) {
      console.log(e);
    }
  }

  const getFood = async (id: number) => {
    return await foodService.getFoodByID(id);
  };

  const foodImage = async (id: number) => {
    return await foodService.getFoodImage(id);
  };

  async function showPopupFood() {
    const res = foodService.getFoodByID(foodSelection.selectedID);
    const food = {} as Food;
    food.id = (await res).data.id;
    food.img = (await res).data.img;
    food.info = (await res).data.info;
    food.name = (await res).data.name;
    food.price = (await res).data.price;
    food.type = (await res).data.type;
    const myObject: Food = (await res).data as Food;
    foodSelected.value = myObject;
    popupManagement.sum = food.price;
    popupManagement.popupMenuIsShow = true;

    // console.log("updatePrice " + food.price);
  }

  // async function getFood(id: number) {
  //   try {
  //     const res = await foodService.getfood(id);
  //     return res;
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }
  return {
    reportBadFood,
    getBadFood,
    getBestFood,
    reportBestFood,
    foods,
    foodTypes,
    getFoods,
    getFood,
    foodImage,
    dialog,
    addFoodTypes,
    getFoodsByType,
    foodsByType,
    editedFood,
    saveFood,
    selectedFoodType,
    editFood,
    toggleFoodStatus,
    deleteFood,
    showPopupFood,
    foodSelected,
    foodStatusFalse,
    deleteDialog,
    deleteItemId,
    showDeleteDialog,
    deleteItemName,
  };
});
