import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type FoodQueue from "@/types/foodQueue";
import foodQueueService from "@/services/foodQueue";
import type OrderItem from "@/types/OrderItem";
import type Food from "@/types/Food";

export const useFoodQueueStore = defineStore("foodQueue", () => {
  const allFoodQueue = ref<FoodQueue[]>([]);
  const myFoodQueue = ref<FoodQueue[]>([]);
  const foodQueueByOrderId = ref<FoodQueue[]>([]);
  const payQueue = ref<FoodQueue[]>([]);

  const chefName = ref("");
  watch(myFoodQueue, async () => {
    await getMyFoodQueue();
  });

  const dialog = ref(false);
  const canCelId = ref(-1);
  const closeMenu = ref(false);
  const canCelFood = ref<Food>();

  //เอา food-queue ทั้งหมด สถานะ = รอทำ
  const allFoodQueueChef = ref<FoodQueue[]>([]);

  // const notiChef = ref(allFoodQueueChef.value.length);

  async function getAllFoodQueue() {
    try {
      const res = await foodQueueService.getFoodQueue();
      allFoodQueue.value = res.data;
      console.log(allFoodQueue.value);
    } catch (e) {
      console.log(e);
    }
  }
  async function getFoodQueueByOrderId(idOrder: string) {
    try {
      const res = await foodQueueService.getFoodQueueByOrderId(idOrder);
      foodQueueByOrderId.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  //เอา food-queue ทั้งหมด สถานะ = รอทำ
  async function getAllFoodQueueChef() {
    try {
      const res = await foodQueueService.getFoodQueueChef();
      allFoodQueueChef.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function getMyFoodQueue() {
    try {
      const res = await foodQueueService.getMyFoodQueue(chefName.value);
      myFoodQueue.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }
  //เอา food-queue ที่เสริฟแล้ว แล้วยังไม่ได้ชำระเงินตาม order นั้น ๆ
  async function getPayFoodQueue(idOrder: any, status: string) {
    try {
      const res = await foodQueueService.getFoodQueueByStatus(idOrder, status);
      payQueue.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function addCancelFood() {
    let newQ: FoodQueue;
    try {
      const res = await foodQueueService.getFoodQueueByID(canCelId.value);
      newQ = res.data;
      canCelFood.value = newQ.orderItem!.food;
    } catch (e) {
      console.log(e);
    }
  }

  async function cancle() {
    let newQ: FoodQueue;
    try {
      const res = await foodQueueService.getFoodQueueByID(canCelId.value);
      newQ = res.data;
      newQ.status = "ยกเลิก";
      // if (closeMenu.value === true) {
      // canCelFood.value = newQ.orderItem!.food;
      // console.log(canCelFood.value);
      // }
      foodQueueService.updateFoodQueue(canCelId.value, newQ);
      //ลบหน้าบ้าน
      const index = allFoodQueueChef.value.findIndex(
        (item) => item.id === canCelId.value
      );
      allFoodQueueChef.value.splice(index, 1);
      //
      // getAllFoodQueueChef();
    } catch (e) {
      console.log(e);
    }
  }

  async function cancleMyFoodQueue(id: number) {
    let newQ: FoodQueue;
    try {
      const res = await foodQueueService.getFoodQueueByID(id);
      newQ = res.data;
      newQ.status = "ยกเลิก";
      foodQueueService.updateFoodQueue(id, newQ);
      // if (closeMenu.value === true) {
      canCelFood.value = newQ.orderItem!.food;
      console.log(canCelFood.value);
      // }
      foodQueueService.updateFoodQueue(canCelId.value, newQ);
      //ลบหน้าบ้าน
      const index = myFoodQueue.value.findIndex((item) => item.id === id);
      myFoodQueue.value.splice(index, 1);
      //
      // getAllFoodQueueChef();
    } catch (e) {
      console.log(e);
    }
  }

  async function finish(id: number) {
    let newQ: FoodQueue;
    try {
      const res = await foodQueueService.getFoodQueueByID(id);
      newQ = res.data;
      newQ.status = "พร้อมเสิร์ฟ";
      foodQueueService.updateFoodQueue(id, newQ);
      //ลบหน้าบ้าน
      const index = myFoodQueue.value.findIndex((item) => item.id === id);
      myFoodQueue.value.splice(index, 1);
      //
      // getAllFoodQueueChef();
    } catch (e) {
      console.log(e);
    }
  }

  async function serveFinish(foodQueue: FoodQueue) {
    try {
      foodQueue.status = "เสิร์ฟสำเร็จ";
      foodQueue.waiterName = localStorage.getItem("currentEmp")?.toString();
      console.log("boy serve");
      console.log(localStorage.getItem("currentEmp")?.toString());
      foodQueueService.updateFoodQueue(foodQueue.id!, foodQueue);
    } catch (e) {
      console.log(e);
    }
  }
  const foodQueueNotUnPaid = ref<FoodQueue[]>([]);

  async function getFoodQueueNotUnPaid(idTable: number) {
    const res = await foodQueueService.getFoodQueueNotUnPaid(idTable);
    foodQueueNotUnPaid.value = res.data;
  }

  async function addMyOrders(id: number) {
    console.log("wอดี" + id);
    let newQ: FoodQueue;
    // chefName = name;
    try {
      console.log("wอดี2" + id);
      const res = await foodQueueService.getFoodQueueByID(id);
      console.log("wอดี3" + id);
      newQ = res.data;
      newQ.chefName = chefName.value;
      newQ.status = "กำลังทำ";
      foodQueueService.updateFoodQueue(id, newQ);
      //ลบหน้าบ้าน
      const index = allFoodQueueChef.value.findIndex((item) => item.id === id);

      // myFoodQueue.value.push(newQ);
      await getMyFoodQueue();

      allFoodQueueChef.value.splice(index, 1);

      //

      // getMyFoodQueue();
    } catch (e) {
      console.log(e);
    }
  }
  return {
    payQueue,
    getPayFoodQueue,
    getAllFoodQueue,
    allFoodQueue,
    getAllFoodQueueChef,
    allFoodQueueChef,
    cancle,
    addMyOrders,
    myFoodQueue,
    getMyFoodQueue,
    cancleMyFoodQueue,
    foodQueueNotUnPaid,
    getFoodQueueNotUnPaid,
    serveFinish,
    finish,
    chefName,
    dialog,
    canCelId,
    closeMenu,
    canCelFood,
    addCancelFood,
    foodQueueByOrderId,
    getFoodQueueByOrderId,
  };
});
