import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import userService from "@/services/user";

export const useUserStore = defineStore("user", () => {
  // const dialog = ref(false);
  // let lastId = 6;
  // const dialog = ref(false);
  // let lastId = 4;
  // const isTable = ref(true);
  // const editedUser = ref<User>({
  //   id: -1,
  //   login: "",
  //   name: "",
  //   password: "",
  //   img: "",
  //   position: "",
  // });
  // const editedUser = ref<User>({
  //   id: -1,
  //   login: "",
  //   name: "",
  //   password: "",
  //   img: "",
  //   position: "",
  // });

  const users = ref<User[]>([]);

  // const users = ref<User[]>([
  //   {
  //     id: 1,
  //     login: "admin",
  //     name: "Administrator",
  //     password: "Pass@1234",
  //     img: "./src/assets/img/admin.png",
  //     position: "เจ้าของร้าน",
  //   },
  //   {
  //     id: 2,
  //     login: "user1",
  //     name: "User 1",
  //     password: "Pass@1234",
  //     img: "./src/assets/img/user1.png",
  //     position: "พนักงาน",
  //   },
  //   {
  //     id: 3,
  //     login: "user2",
  //     name: "User 2",
  //     password: "Pass@1234",
  //     img: "./src/assets/img/user2.png",
  //     position: "พนักงาน",
  //   },

  //   {
  //     id: 4,
  //     login: "arthit",
  //     name: "Arthit",
  //     password: "Arthit@2545",
  //     img: "./src/assets/img/arthit.png",
  //     position: "พนักงาน",
  //   },

  //   {
  //     id: 5,
  //     login: "thanwa",
  //     name: "Thanwa",
  //     password: "Thanwa@1234",
  //     img: "./src/assets/img/user1.png",
  //     position: "พนักงาน",
  //   },
  // ]);

  const login = (loginName: string, password: string): User => {
    const index = users.value.findIndex((item) => item.login === loginName);
    if (index >= 0) {
      const user = users.value[index];
      if (user.password === password) {
        return user;
      }
      return user;
    }
    return {
      id: 0,
      login: "",
      name: "",
      password: "",
      img: "",
      position: "",
    };
  };

  // const deleteUser = (id: number): void => {
  //   const index = users.value.findIndex((item) => item.id === id);
  //   users.value.splice(index, 1);
  // };

  // const editUser = (user: User) => {
  //   editedUser.value = { ...user };
  //   // editedUser.value = JSON.parse(JSON.stringify(user));
  //   dialog.value = true;
  // };

  // const saveUser = () => {
  //   if (editedUser.value.id < 0) {
  //     editedUser.value.id = lastId++;
  //     users.value.push(editedUser.value);
  //   } else {
  //     const index = users.value.findIndex(
  //       (item) => item.id === editedUser.value.id
  //     );
  //     users.value[index] = editedUser.value;
  //   }

  //   dialog.value = false;
  //   clear();
  // };

  // const clear = () => {
  //   editedUser.value = {
  //     id: -1,
  //     login: "",
  //     name: "",
  //     password: "",
  //     img: "",
  //     position: "",
  //   };
  // };

  // return {
  //   users,
  //   login,
  //   deleteUser,
  //   editUser,
  //   saveUser,
  //   editedUser,
  //   clear,
  //   dialog,
  // };

  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);

  const editedUser = ref<User & { files: File[] }>({
    login: "",
    name: "",
    password: "",
    img: "no-user-image.jpg",
    position: "",
    files: [],
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedUser.value = {
        login: "",
        name: "",
        password: "",
        img: "no-user-image.jpg",
        position: "",
        files: [],
      };
    }
  });
  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUsers();
      users.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        const res = await userService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await userService.saveUser(editedUser.value);
      }

      dialog.value = false;
      await getUsers();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก User ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteUser(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await userService.daleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ User ได้");
    }
    loadingStore.isLoading = false;
  }
  function editUser(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }
  return {
    users,
    getUsers,
    dialog,
    editedUser,
    saveUser,
    editUser,
    deleteUser,
    login,
  };
});
