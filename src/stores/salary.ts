import { ref, watch } from "vue";
import { defineStore } from "pinia";
import salaryService from "@/services/salary";
import type Salary from "@/types/salary";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type CheckInOut from "@/types/checkInOut";
import checkInOutService from "@/services/checkInOut";
import employeeService from "@/services/employee";
import type Employee from "@/types/employee";
export const useSalaryStore = defineStore("salary", () => {
  const salarys = ref<CheckInOut[]>([]);
  const emp = ref<Employee[]>([]);
  const empName = ref<string[]>([]);
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const nameEmp = ref(["ทั้งหมด"]);
  const selectedNameEmp = ref(nameEmp.value[0]);
  //For first time when logged id.
  async function getSalary() {
    try {
      const res = await salaryService.getSalary();
      salarys.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึง Salary ได้");
    }
  }
  watch(selectedNameEmp, async () => {
    sumBalance.value = 0;
    sumhour.value = 0;
    await getEmpByName();
  });
  const calHour = (): number => {
    console.log(empName.value[0]);
    let hour = 0;
    salarys.value.forEach((element) => {
      if (
        element.employee?.firstname === selectedNameEmp.value &&
        element.status === "ยังไม่ชำระ"
      ) {
        hour += element.hour!;
        console.log(element);
      } else {
        if (element.status === "ยังไม่ชำระ") {
          hour += element.hour!;
        }
      }
    });
    return hour;
  };

  const calBalance = (): number => {
    let balance = 0;
    salarys.value.forEach((element) => {
      if (
        element.employee?.firstname === selectedNameEmp.value &&
        element.status === "ยังไม่ชำระ"
      ) {
        balance += element.hour! * element.employee!.hourrate;
      } else {
        if (element.status === "ยังไม่ชำระ") {
          balance += element.hour! * element.employee!.hourrate;
        }
      }
    });
    return balance;
  };
  async function getEmpByName() {
    if (selectedNameEmp.value === "ทั้งหมด") {
      await getSalary();
    } else {
      try {
        const res = await salaryService.getSalaryByName(selectedNameEmp.value);
        salarys.value = res.data;
        console.log(res.data);
      } catch (e) {
        console.log(e);
      }
    }
  }
  async function getEmp() {
    empName.value = [];
    empName.value.push("ทั้งหมด");
    try {
      const res = await employeeService.getEmployee();
      emp.value = res.data;
      emp.value.forEach((element) => {
        empName.value.push(element.firstname.toString());
      });
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึง Salary ได้");
    }
  }

  async function deleteSalary(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await salaryService.daleteSalary(id);
      await getSalary();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Salary ได้");
    }
    loadingStore.isLoading = false;
  }

  async function payByIndividual(checkInOut: CheckInOut) {
    const newSalary: Salary = {
      employee: checkInOut.employee,
      salary: checkInOut.employee!.hourrate * checkInOut.hour!,
      date: new Date(),
    };
    const newChekInOut: CheckInOut = {
      status: "ชำระแล้ว",
    };
    const res = await salaryService.saveSalary(newSalary);
    console.log(res);
    checkInOutService.updateCheckInOut(checkInOut.id!, newChekInOut);
  }

  async function payAll() {
    salarys.value.forEach(async (checkInOut) => {
      if (checkInOut.status === "ยังไม่ชำระ") {
        const newSalary: Salary = {
          employee: checkInOut.employee,
          salary: checkInOut.employee!.hourrate * checkInOut.hour!,
          date: new Date(),
        };
        const newChekInOut: CheckInOut = {
          status: "ชำระแล้ว",
        };
        checkInOutService.updateCheckInOut(checkInOut.id!, newChekInOut);
        const res = await salaryService.saveSalary(newSalary);
      }
    });
  }

  function havePayMent(): boolean {
    let count = 0;
    salarys.value.forEach((element) => {
      if (element.status === "ยังไม่ชำระ") {
        count++;
      }
    });
    if (count === 0) {
      return false;
    }
    return true;
  }

  const selectedEmp = ref(empName.value[0]);
  const sumhour = ref(0);
  const sumBalance = ref(0);
  return {
    dialog,
    nameEmp,
    selectedEmp,
    havePayMent,
    payAll,
    getSalary,
    payByIndividual,
    salarys,
    deleteSalary,
    getEmp,
    emp,
    empName,
    selectedNameEmp,
    getEmpByName,
    sumhour,
    sumBalance,
    calHour,
    calBalance,
  };
});
