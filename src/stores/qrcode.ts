import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useQrCodeStore = defineStore("qrcode", () => {
  const scanQR = ref(false);
  const link = ref("");
  const orderUUID = ref("");
  const position = ref("");

  return {
    scanQR,
    link,
    orderUUID,
    position,
  };
});
