import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Material from "@/types/material";
import materialService from "@/services/material";
import { useSelectStore } from "./selectMenu";
import { usemanagePopupStore } from "@/stores/popupManagement";

export const useMaterialStore = defineStore("material", () => {
  const popupManagement = usemanagePopupStore();
  const dialog = ref(false);
  const addMaterialUnits = ref(["ฟอง", "กิโลกรัม", "ชิ้น", "ลิตร"]);
  const materials = ref<Material[]>([]);
  const materialSelected = ref<Material>();
  const materialsByUnit = ref<Material[]>([]);
  const editedMaterial = ref<Material>({
    name: "",
    amount: 0,
    price: 0,
    unit: "",
  });
  watch(materials, () => {
    getMaterials();
  });
  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedMaterial.value = {
        name: "",
        amount: 0,
        price: 0,
        unit: "",
      };
    }
  });

  //For first time when logged id.
  async function getMaterials() {
    try {
      const res = await materialService.getMaterials();
      materials.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function saveMaterial() {
    try {
      //use when edit material
      if (editedMaterial.value.id) {
        const res = await materialService.updateMaterial(
          editedMaterial.value.id,
          editedMaterial.value
        );
      }
      //use when add new material
      else {
        const res = await materialService.saveMaterial(editedMaterial.value);
      }
      dialog.value = false;
      await getMaterials();
    } catch (e) {
      console.log(e);
    }
  }

  function editMaterial(material: Material) {
    editedMaterial.value = JSON.parse(JSON.stringify(material));
    dialog.value = true;
  }

  async function deleteMaterial(id: number) {
    try {
      const res = await materialService.daleteMaterial(id);
      await getMaterials();
    } catch (e) {
      console.log(e);
    }
  }

  const getMaterial = async (id: number) => {
    return await materialService.getMaterialByID(id);
  };

  return {
    addMaterialUnits,
    materials,
    getMaterials,
    getMaterial,
    dialog,
    materialsByUnit,
    editedMaterial,
    saveMaterial,
    editMaterial,
    deleteMaterial,
    materialSelected,
  };
});
