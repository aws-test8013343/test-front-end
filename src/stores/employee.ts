import { ref, watch } from "vue";
import { defineStore } from "pinia";
import employeeService from "@/services/employee";
import type Employee from "@/types/employee";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useEmployeeStore = defineStore("employee", () => {
  const employess = ref<Employee[]>([
    {
      age: 10,
      email: "sds",
      firstname: "",
      hourrate: 0,
      birthday: "",
      identityNum: "",
      lastname: "",
      role: "",
    },
  ]);

  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);

  // const employeeTitle = ref("จัดการพนักงาน");

  const editedEmployee = ref<Employee>({
    firstname: "",
    lastname: "",
    age: 0,
    identityNum: "",
    email: "",
    birthday: "",
    role: "",
    hourrate: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedEmployee.value = {
        firstname: "",
        lastname: "",
        age: 0,
        identityNum: "",
        email: "",
        birthday: "",
        role: "",
        hourrate: 0,
      };
    }
  });

  //For first time when logged id.
  async function getEmployees() {
    try {
      const res = await employeeService.getEmployee();
      employess.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }
  const getEmployee = async (id: number) => {
    return await employeeService.getEmployeeByID(id);
  };

  async function saveEmployee() {
    loadingStore.isLoading = true;
    try {
      if (editedEmployee.value.id) {
        const res = await employeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        const res = await employeeService.saveEmployee(editedEmployee.value);
      }

      dialog.value = false;
      await getEmployees();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Employee ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteEmployee(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.daleteEmployee(id);
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  function editEmployeekub(employee: Employee) {
    editedEmployee.value = JSON.parse(JSON.stringify(employee));
    dialog.value = true;
  }
  return {
    dialog,
    getEmployees,
    employess,
    getEmployee,
    editedEmployee,
    deleteEmployee,
    saveEmployee,
    editEmployeekub,
  };
});
