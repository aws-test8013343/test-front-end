import { createRouter, createWebHistory } from "vue-router";
import MenuView from "../views/MenuView.vue";
import TableViewT from "../views/TableViewT.vue";
import LoginView from "@/views/LoginView.vue";
import TableView from "@/views/TableView.vue";
import MyDrawer from "@/components/menus/MyDrawer.vue";
import ServeView from "@/views/ServeView.vue";
import CookView from "@/views/CookView.vue";
import CashierView from "@/views/CashierView.vue";
import UserView from "@/views/UserView.vue";
import EmployeeView from "@/views/EmployeeView.vue";
import SalaryView from "@/views/SalaryView.vue";
import FoodView from "@/views/FoodView.vue";
import ManageTable from "@/views/ManageTableView.vue";
import ReportView from "@/views/ReportView.vue";
import CheckInOut from "@/components/CheckInOut.vue";
import ManageMaterial from "@/views/ManageMaterialView.vue";
import QrcodeReaderVue from "@/views/QrcodeReaderView.vue";
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/login",
      name: "mainlogin",
      component: () => LoginView,
    },

    {
      path: "/",
      name: "login",
      component: () => LoginView,
    },

    {
      path: "/menu",
      name: "menu",
      components: {
        default: MenuView,
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/menuCus/:order",
      name: "menuCus",
      components: {
        default: MenuView,
        menu: () => MyDrawer,
      },
      meta: {
        layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/menu/:order",
      name: "menu",
      components: {
        default: MenuView,
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },

    {
      path: "/table",
      name: "table",
      components: {
        default: () => TableView,
        menu: () => MyDrawer,
      },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/serve",
      name: "serve",
      components: { default: ServeView, menu: () => MyDrawer },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },

    {
      path: "/cook",
      name: "cook",
      components: { default: CookView, menu: () => MyDrawer },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/cashier",
      name: "cashier",
      components: { default: CashierView, menu: () => MyDrawer },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/trolley/:order",
      name: "trolley",
      component: () => import("../views/TrolleyView.vue"),
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/trolleyCus/:order",
      name: "trolleyCus",
      component: () => import("../views/TrolleyView.vue"),
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/order/:order",
      name: "order",
      component: () => import("../views/OrderView.vue"),
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/orderCus/:order",
      name: "orderCus",
      component: () => import("../views/OrderView.vue"),
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/user",
      name: "user",
      components: { default: UserView, menu: () => MyDrawer },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/employee",
      name: "employee",
      components: { default: EmployeeView, menu: () => MyDrawer },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/salary",
      name: "salary",
      components: { default: SalaryView, menu: () => MyDrawer },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/food",
      name: "food",
      components: { default: FoodView, menu: () => MyDrawer },
      meta: {
        layout: "MainLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/manageTable",
      name: "manageTable",
      components: { default: ManageTable, menu: () => MyDrawer },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/manageMaterial",
      name: "manageMaterial",
      components: { default: ManageMaterial, menu: () => MyDrawer },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/checkInOut",
      name: "checkInOut",
      components: { default: CheckInOut, menu: () => MyDrawer },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/report",
      name: "report",
      components: { default: ReportView, menu: () => MyDrawer },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
  ],
});

function isLogin() {
  const user = localStorage.getItem("user");
  if (user) {
    return true;
  }
  return false;
}

router.beforeEach((to, from) => {
  // instead of having to check every route record with
  // to.matched.some(record => record.meta.requiresAuth)
  if (to.meta.requiresAuth && !isLogin()) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    return {
      path: "/login",
      // save the location we were at to come back later
      query: { redirect: to.fullPath },
    };
  }
});

export default router;
